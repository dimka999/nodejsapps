import React from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  Text,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import data from './../../../data/data.json';
const {height, width} = Dimensions.get('window');

const List = props => {
  const superProps = props.props;

  function Item({item}) {
    _onPress = item => {
      superProps.about(item.id, item.title, item.description);
    };
    return (
      <TouchableOpacity onPress={() => this._onPress(item)}>
        <View style={styles.item}>
          <Text style={styles.textStyles}>{item.title}</Text>
        </View>
      </TouchableOpacity>
    );
  }

  return (
    <View style={styles.container}>
      <FlatList
        data={data}
        renderItem={({item}) => <Item item={item} />}
        keyExtractor={item => item.id.toString()}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 8,
    height: height * 0.88,
  },
  item: {
    minHeight: 40,
    height: 'auto',
    padding: 5,
    marginVertical: 4,
    marginHorizontal: 6,
    borderColor: '#D3D3D3',
    borderWidth: 1,
  },
  textStyles: {
    color: '#000',
    fontSize: 18,
  },
});

export default List;
