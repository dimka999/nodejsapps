import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

const Header = props => {
  const superProps = props.props;
  const {
    store: {title, user, isLoggedIn, iconShow},
  } = props.props;

  _onPress = () => {
    superProps.backPage();
  };

  return (
    <View style={styles.viewStyles}>
      <StatusBar backgroundColor="red" />
      <View style={styles.leftIconContainer}>
        {iconShow ? (
          <TouchableOpacity
            style={styles.leftIconStyle}
            onPress={() => this._onPress()}>
            <Icon name="arrowleft" size={26} color="#fff" />
          </TouchableOpacity>
        ) : null}
        {title ? <Text style={styles.LeftTextStyles}>{title}</Text> : null}
      </View>
      {isLoggedIn ? (
        <View style={styles.iconUserStyle}>
          <Text style={styles.textUserStyles}>{user}</Text>
          <Icon name="user" size={26} color="#fff" />
        </View>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  viewStyles: {
    backgroundColor: 'red',
    padding: 0,
    height: 40,
    justifyContent: 'space-between',
    paddingTop: 7,
    paddingLeft: 8,
    shadowColor: '#000',
    elevation: 5,
    flexDirection: 'row',
  },
  leftIconContainer: {
    flexDirection: 'row',
  },
  LeftTextStyles: {
    color: '#fff',
    fontSize: 16,
  },
  textUserStyles: {
    color: '#fff',
    fontSize: 16,
  },
  leftIconStyle: {
    paddingRight: 10,
  },
  iconUserStyle: {
    paddingRight: 8,
    flexDirection: 'row',
  },
});

export default Header;
