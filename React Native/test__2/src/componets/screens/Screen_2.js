import React from 'react';
import {View} from 'react-native';
import Header from './../uikit/Header';
import List from './../uikit/List';

const Screen_2 = props => {
  const superProps = props.props;

  return (
    <View>
      <Header props={superProps} />
      <List props={superProps} />
    </View>
  );
};

export default Screen_2;
