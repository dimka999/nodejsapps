import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  ScrollView,
  Dimensions,
} from 'react-native';
import Header from './../uikit/Header';
const {height, width} = Dimensions.get('window');

const Screen_3 = props => {
  const superProps = props.props;
  const title = superProps.store.about;
  const description = superProps.store.description;

  _onPress_back = () => {
    superProps.backPage();
  };
  _onPress_logout = () => {
    superProps.logout();
  };

  return (
    <View>
      <Header props={superProps} />
      <View>
        <ScrollView>
          <View style={styles.TitleContainer}>
            <Text style={styles.TitleContainerText}>{title}</Text>
            <Text style={styles.TitleContainerLongText}>{description}</Text>
          </View>
        </ScrollView>
        <View style={styles.FooterContainer}>
          <View style={styles.ButtonContainer}>
            <Button
              style={styles.Button}
              color="red"
              title="Назад"
              onPress={() => this._onPress_back()}
            />
          </View>
          <View style={styles.ButtonContainer}>
            <Button
              style={styles.Button}
              color="red"
              title="Выйти из аккаунта"
              onPress={() => this._onPress_logout()}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  TitleContainer: {
    height: height * 0.82,
    justifyContent: 'flex-start',
    paddingTop: 10,
    padding: 5,
    flex: 1,
  },
  TitleContainerText: {
    color: 'grey',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'left',
  },
  TitleContainerLongText: {
    paddingTop: 5,
    fontSize: 16,
    color: 'grey',
    textAlign: 'left',
  },
  FooterContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 4,
  },
  ButtonContainer: {
    width: '49.5%',
  },
  Button: {},
});

export default Screen_3;
