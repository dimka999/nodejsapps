import React from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Button,
  Dimensions,
  KeyboardAvoidingView,
} from 'react-native';
import Header from './../uikit/Header';
const {height, width} = Dimensions.get('window');

const Screen_1 = props => {
  let login;
  const superProps = props.props;

  return (
    <View>
      <Header props={superProps} />
      <KeyboardAvoidingView
        behavior="position"
        keyboardVerticalOffset="40"
        enabled>
        <View style={styles.TitleContainer}>
          <Text style={styles.TitleContainerText}>ВХОД</Text>
          <Text style={styles.TitleContainerLongText}>
            Согласно классификации М.Вебера, форма политического сознания
            предсказуема. Политическая психология, согласно традиционным
            представлениям, символизирует системный культ личности
          </Text>
        </View>
        <View style={styles.InputsContainer}>
          <TextInput
            style={styles.TextInputStyle}
            placeholder="Логин"
            underlineColorAndroid="transparent"
            onChangeText={text => {
              login = text;
            }}
          />
          <TextInput
            style={styles.TextInputStyle}
            placeholder="Пароль"
            underlineColorAndroid="transparent"
            secureTextEntry={true}
          />
          <View style={styles.ButtonContainer}>
            <Button
              color="red"
              title="Войти"
              onPress={() => {
                if (login) {
                  superProps.logined(login);
                }
              }}
            />
          </View>
        </View>
      </KeyboardAvoidingView>
    </View>
  );
};

const styles = StyleSheet.create({
  TitleContainer: {
    height: height * 0.6,
    justifyContent: 'center',
  },
  TitleContainerText: {
    color: 'red',
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  TitleContainerLongText: {
    fontSize: 18,
    color: 'grey',
    textAlign: 'center',
  },
  InputsContainer: {
    alignContent: 'flex-start',
    borderColor: '#D3D3D3',
  },
  TextInputStyle: {
    textAlign: 'left',
    marginBottom: 7,
    height: 40,
    borderBottomWidth: 1,
    borderColor: '#D3D3D3',
    marginHorizontal: 10,
    paddingHorizontal: 10,
  },
  ButtonContainer: {
    paddingHorizontal: width * 0.2,
  },
});

export default Screen_1;
