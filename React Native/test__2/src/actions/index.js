export const logined = user => {
  return {
    type: 'PAGE_2',
    user,
  };
};
export const about = (id, title, description) => {
  return {
    type: 'PAGE_3',
    id,
    title,
    description,
  };
};
export const backPage = () => {
  return {
    type: 'PAGE_2',
  };
};
export const logout = () => {
  return {
    type: 'PAGE_1',
  };
};
