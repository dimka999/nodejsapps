const init_state = {
  screen: 'Screen_1',
  title: 'Вход в личный кабинет',
  isLoggedIn: false,
  iconShow: false,
};

const reducer = (state = init_state, action) => {
  switch (action.type) {
    case 'PAGE_1':
      return init_state;
    case 'PAGE_2':
      return {
        screen: 'Screen_2',
        title: 'Список',
        user: state.user ? state.user : action.user,
        isLoggedIn: true,
        iconShow: false,
      };
    case 'PAGE_3':
      return {
        screen: 'Screen_3',
        title: 'Подробнее',
        user: state.user,
        isLoggedIn: state.isLoggedIn,
        iconShow: true,
        about: action.title,
        description: action.description,
      };
    default:
      return state;
  }
};

export default reducer;
