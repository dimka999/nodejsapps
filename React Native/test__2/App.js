import React, {Component} from 'react';
import {SafeAreaView} from 'react-native';
import {connect} from 'react-redux';

import {logined, about, backPage, logout} from './src/actions';

import Screen_1 from './src/componets/screens/Screen_1';
import Screen_2 from './src/componets/screens/Screen_2';
import Screen_3 from './src/componets/screens/Screen_3';

class App extends Component {
  render() {
    const {
      props: store,
      props: {
        store: {screen},
      },
    } = this;

    return (
      <SafeAreaView>
        {screen === 'Screen_1' ? <Screen_1 props={store} /> : null}
        {screen === 'Screen_2' ? <Screen_2 props={store} /> : null}
        {screen === 'Screen_3' ? <Screen_3 props={store} /> : null}
      </SafeAreaView>
    );
  }
}

const mapStateToProps = store => {
  return {
    store: store,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch(logout()),
    logined: login => dispatch(logined(login)),
    about: (id, title, description) => dispatch(about(id, title, description)),
    backPage: () => dispatch(backPage()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
