const express = require('express');
const port = 3000;
const app = express();
const request = require('request');

app.set('view engine', 'pug')
app.use('/', function(req, res) {
	
	let dataUsers = [];
	
	// создаем построчно таблицу для рендеринка в PUG
	function writeTabale(body) {
		let usersObj = JSON.parse(body); 	
		let usersToPrint = [];
		let currentRow = 0; // текущая строка в целевой таблице
		
		for (let row in usersObj["user"]) {
			let count_book	= 0;
			
			usersToPrint.push({});	// 			
			usersToPrint[currentRow].name = usersObj["user"][row].name;
			usersToPrint[currentRow].old =  usersObj["user"][row].old;	
			
			for (let col in usersObj["book"]) {
				if (usersObj["user"][row].uid == usersObj["book"][col].uid) {
					count_book++;
					if (count_book == 1) {	
						usersToPrint[currentRow].title = usersObj["book"][col].title;
						usersToPrint[currentRow].year = usersObj["book"][col].year;	 
						
					} else if (count_book > 1 ){
						currentRow++;					
						usersToPrint.push({});
						usersToPrint[currentRow].name = '';
						usersToPrint[currentRow].old =  '';		 							
						usersToPrint[currentRow].title = usersObj["book"][col].title;
						usersToPrint[currentRow].year = usersObj["book"][col].year;																	
					}								
				} 	
			} 
			if (count_book == 0){
				usersToPrint[currentRow].name.title = '';
				usersToPrint[currentRow].name.year = '';	 					
			};				
			currentRow++;		
		}	
		return usersToPrint;
	};	
	
	// получаем данный по url
	function getJson(resolve, reject) {
		let url = 'http://z.bokus.ru/user.json';				
		
		let httpGet = new Promise(function(resolve, reject) {
			request(url, (err, response, body) => {
				if (err) { 					
					return res.status(500).send({ message: err });		
				};
				resolve(body);				
			});			
		});	
		
		httpGet.then(writeTabale,
			(err) => console.log(err)  
		).then(users => {			
			dataUsers = users;
			render(dataUsers);					
		}, (err) =>  render(dataUsers))	
	}	

	function render(dataUsers) {	
		res.render('index', {
			title: 'test_1',
			users: dataUsers
		}) 			
	}
	
	getJson();	
})

app.get('/', (req, res) => {

	console.log(`Done`);
})

app.listen(port, function() {
  console.log(`Example listening on port ${port}`);
});
  