CREATE OR ALTER PROCEDURE [bulkIncertCvsProc]
(	
	@data_file varchar(255), -- путь к файлу
	@tableName varchar(255), 
	@fieldterminator varchar(10) = ',', --разделитель
	@rowterminator varchar(10) = '\n', --признак конца строки		
	@errorfile varchar(255) = '', -- путь к файлу логов
	@first_row varchar(10) = 2	
)
AS
BEGIN
	DECLARE @bulkQuery NVARCHAR(MAX);
		 
	set @bulkQuery = 'BULK INSERT ' + @tableName + ' FROM ''' + @data_file + ''' WITH (fieldterminator = ''' + @fieldterminator + ''',ROWTERMINATOR = ''' + @rowterminator +''',FIRSTROW = '+ @first_row +',ERRORFILE = ''' + @errorfile +'''); SELECT * FROM ' + @tableName;

	BEGIN TRY  
		IF OBJECT_ID(@tableName,'U') IS NULL 
			BEGIN 
				PRINT N'Создаем таблицу ' + @tableName 
				EXECUTE ('							
					CREATE TABLE ' + @tableName + '(
					  [ID_Country] varchar(50),
					  [ID_Plant] varchar(50),
					  [ID_Sold-to party] varchar(50),
					  [ID_Ship-to Party] varchar(50),
					  [ID_PP] varchar(50),
					  [Calendar Day] varchar(50),
					  [Billing document] varchar(50),
					  [Reference document] varchar(50),
					  [Intercompany Ind] varchar(50),
					  [Inv (CS)] varchar(50),
					  [Inv (SU)] varchar(50),
					  [Inv (ISV Mkt)] varchar(50),
					  [Inv (M3)] varchar(50),
					  [Inv (USD)] varchar(50),
					  [Inv (PC)] varchar(50)
					 );'
				);	
				PRINT N'Таблица ' + @tableName + N' успешно создана, начинаем импорт'; 
			END 
		ELSE PRINT N'Таблица ' + @tableName + N' в базе найдена, начинаем импорт';		
			
		EXECUTE (@bulkQuery);		
		PRINT NCHAR(13) + N'Данные из файла '+ @data_file +N' успешно импортированы';

	 END TRY  
	 BEGIN CATCH  	 	
		SELECT	ERROR_MESSAGE() AS ErrorMessage		
	 END CATCH	
END